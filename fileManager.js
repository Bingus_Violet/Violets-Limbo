const chokidar = require("chokidar"),
path = require("path"),
fs = require("fs"),
EventEmitter = require("events").EventEmitter

var dataPath = path.join(__dirname, 'data')
var postsPath = path.join(dataPath, 'posts')
var staticPath = path.join(__dirname, 'static')

var reqPaths = [dataPath, postsPath]

for (var i = 0; i < reqPaths.length; i++) {
    var p = reqPaths[i]
    if (!fs.existsSync(p)) {
        fs.mkdirSync(p)
    }
}

module.exports = {
    data: dataPath,
    posts: postsPath,
    static: staticPath,
    emitter: new EventEmitter()
}

var watcher = chokidar.watch(dataPath)

watcher
    .on('change', module.exports.emitter.emit)
    .on('add', module.exports.emitter.emit)