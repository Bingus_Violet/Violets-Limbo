# Violet's Limbo
Violet's Limbo is my own blog posting software, not made for commercial use.

## How does it work?
Within `data.json`, I can specify the title & path of posts (more planned in the future) in an array, and the order of the posts is based on the order of the elements in the array. Janky? Yes. Functional? Yes!

## To-Do
- [x] Fix the css on list elements (like these lol)
- [x] Allow pinning of posts (like this one)
- [ ] Add table of contents to posts