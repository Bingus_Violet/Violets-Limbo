const express = require("express"),
paths = require("./fileManager.js")
path = require("path"),
fs = require("fs"),
WebSocket = require("ws")

const PORT = process.env.PORT || 8080

var app = express()

app.use(express.static(paths.data))

app.listen(PORT, () => {
    console.log("Violet's Limbo is now listening on port: " + PORT)
})

var sockets = []

wsServer = WebSocket.Server;
let server = require('http').createServer()
wsServer = new wsServer({
    server: server,
    perMessageDeflate: false
})
server.on('request', app)

wsServer.on("connection", function connection(socket) {
    socket.on('message', function message(data) {
        data = JSON.parse(data)
        if (data.type == "ping") {
            for (let index = 0; index < sockets.length; index++) {
                const socketData = sockets[index];
                if (socketData.socket == socket) {
                    sockets[index].lastPing = Date.now()
                }
            }

            socket.send(`{"op": 3}`)
        } else {
            console.log(data)
        }
    })

    sockets.push({ socket, lastPing: Date.now() })
})


module.exports = {
    app: app
}

require("./api.js")