const express = require("express"),
fs = require("fs"),
path = require("path")

require("./exposer.js")

process.on('uncaughtException', (err, origin) => {
    fs.writeSync(
      process.stderr.fd,
      `Caught exception: ${err}\n` +
      `Exception origin: ${origin}`,
    );
  });